const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const fs = require('fs');

const app = express();
app.use(cors());
app.use(express.static('front'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// definim rutes de la API
app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API de Cites!</h1>");
});

// genera i retorna cita ràndom
app.get("/api/citarandom", (req, res) => {
    let raw = fs.readFileSync('cites.json')
    let cita = JSON.parse(raw);
    var random = Math.floor(Math.random() * (cita.length - 1));
    // console.log(cita[random])
    res.send(cita[random])
});

// set port, listen for requests
const port = process.env.PORT || 3000; //si no existe env.PORT asigna 3000
app.listen(port, () => console.log("Listening on port " + port));